<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:03:38+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['eventIdWithTimestamp']['0'] = 'Eveniment';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['eventIdWithTimestamp']['1'] = 'Tscherna l\'eveniment per includer.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['eventTemplate']['0']        = 'Template d\'eveniment';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['eventTemplate']['1']        = 'Tscherna il template per l\'eveniment.';
