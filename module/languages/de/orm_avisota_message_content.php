<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:52+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['eventIdWithTimestamp']['0'] = 'Event';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['eventIdWithTimestamp']['1'] = 'Bitte wählen Sie aus, welcher Event eingebunden werden soll.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['eventTemplate']['0']        = 'Event-Template';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['eventTemplate']['1']        = 'Bitte wählen Sie das Template für die Darstellung des Events aus.';
